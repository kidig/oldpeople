﻿# coding: utf-8
import logging
import re
import json
from logging import Formatter, FileHandler
from flask import Flask, render_template, request, flash, redirect, url_for
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy import func
from sqlalchemy.sql import label
from pluralize import pluralize

app = Flask(__name__)
app.config.from_object('config')
app.template_filter('pluralize')(pluralize)

db = SQLAlchemy(app)

from app.models import Birthday
from app.utils import allowed_file, load_users


@app.errorhandler(404)
def not_found(err):
    ''' Handler for 404 errors '''
    app.logger.error('404: %s' % (err))
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_error(err):
    ''' Handler for 500 errors '''
    app.logger.error('500: %s' % (err))
    return render_template('500.html'), 500

	
@app.route('/')
def show_users():
    ''' Main page '''
    age_groups = db.session.query(
        label('age', func.year(func.now())-func.year(Birthday.birthday)),
        label('cnt', func.count())
    ).group_by(func.year(Birthday.birthday)).all()

    return render_template('index.html', stats=age_groups)

	
@app.route('/search')
def search_users():
    ''' Handler for user search results '''
    name = request.args.get('name')

    founded = []

    if name:
        founded = db.session.query(
            Birthday.id,
            Birthday.name, 
            Birthday.birthday,
            label('age', func.year(func.now())-func.year(Birthday.birthday))
        ).filter(Birthday.name.like(unicode(name) + '%')).order_by(Birthday.name, Birthday.birthday).all()

    return render_template('search_block.html', items=founded)

	
@app.route('/upload', methods=['POST'])
def upload_users():
    ''' File Upload Handler '''
    datafile = request.files.get('file')
    drop_exists_data = request.form.get('drop_exists')
	
    if datafile and allowed_file(datafile.filename, app.config['ALLOWED_EXTENSIONS']):
        try:
            if drop_exists_data:
                Birthday.query.delete()
            
            for user in load_users(datafile):
                db.session.add(Birthday(*user))

            db.session.commit()
            flash(u'Загрузка успешно завершена')
        except Exception, err:
            app.logger.error('Error while load users: %s' % (err))
            db.session.rollback()
            flash(u'Ошибка загрузки данных')
    else:
        flash(u'Файл для загрузки отсутствует или несоответствует текстовому формату')
		
    return redirect(url_for('show_users'))

	
@app.route('/parser', methods=['GET', 'POST'])
def show_parser():
    ''' Parser Page '''
    if request.method == 'POST':
        text = request.form.get('text') or '{}'
        headers = []
        
        try:
            text = json.loads(text)
	
            if len(text):
                text = text.decode('string_escape')
                headers = re.findall(r"(?P<name>.*?): (?P<value>.*?)\r?\n", text)
        except ValueError, err:
            app.logger.error("JSON Error: %s" % (err))

        return render_template('parser_results.html', items=headers)
	
    return render_template('parser.html')
	
	
@app.teardown_request
def shutdown_session(exception=None):
    ''' Close session after request teardown '''
    db.session.remove()


if not app.debug:
    file_handler = FileHandler('error.log')
    file_handler.setFormatter(Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
