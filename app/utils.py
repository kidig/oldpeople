﻿from datetime import datetime


def allowed_file(filename, allowed):
    ''' check for allowed file extension '''
    return '.' in filename and filename.rsplit('.', 1)[1] in allowed
	

def load_users(data):
    ''' Load user name and birthday from csv file '''
    for line in data:
        if ';' in line:
            name, birthday = line.strip().split(';')
            yield (name, datetime.strptime(birthday, '%d.%m.%Y'))
