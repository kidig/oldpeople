import os

basedir = os.path.abspath(os.path.dirname(__file__))


DEBUG = True

SECRET_KEY = 'MyReallySecretKeyForThisApp$$'

#SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_DATABASE_URI = 'mysql://username:passwd@localhost/oldpeople?charset=utf8'

DATABASE_CONNECT_OPTIONS = {}

ALLOWED_EXTENSIONS = set(['csv', 'txt'])


