=Тестовое задание


==Инструкция по запуску:

1. Устанавливаем пакеты: python, python-virtualenv, python-pip, python-dev

2. Запускаем среду virtualenv и активируем её через:
 
    source path/to/venv/bin/activate

3. Устанавливаем зависимости через pip install requirements.txt

4. Настраиваем базу данных MySQL. При необходимости создаем пользователя и новую БД командой:

    CREATE DATABASE oldpeople

   (или используем любую существующую базу и пользователя)

5. Настройки подключения к базе данных указываем в файле config.py:

    SQLALCHEMY_DATABASE_URI = 'mysql://username:passwd@localhost/oldpeople?charset=utf8'бко

6. Создаем схему данных командой: python init_db.py

7. Запускаем тестовое окружение: python run.py

8. Открываем приложение в браузере: http://localhost:8000


== Обратная связь

При обнаружении ошибок, а также чтобы задать все интересующие вопросы - прошу писать на me@myexample.ru
c пометкой "oldpeople", либо оставлять комментарии на https://bitbucket.org/kidig/oldpeople

