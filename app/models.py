from app import db


class Birthday(db.Model):
    __tablename__ = 'birthday'
    
    __table_args__ = {
        'mysql_charset': 'utf8'
    }

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), index=True)
    birthday = db.Column(db.DateTime(), index=True)


    def __init__(self, name=None, birthday=None):
        self.name = name
        self.birthday = birthday


    def __repr__(self):
        return '<Birthday %r>' % (self.name)
